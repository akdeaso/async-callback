import Grid from "./gridModule.js";

fetch("https://jsonplaceholder.typicode.com/users")
  .then((response) => response.json())
  .then((data) => {
    const gridData = {
      columns: ["ID", "Name", "Username", "Email", "Address", "Company"],
      data: data.map((user) => [
        user.id,
        user.name,
        user.username,
        user.email,
        `${user.address.street}, ${user.address.suite}, ${user.address.city}`,
        user.company.name,
      ]),
    };

    const grid = new Grid(gridData);
    grid.render("grid");
  })
  .catch((error) => {
    console.error("Error fetching data:", error);
  });
