class Grid {
  constructor(options) {
    this.columns = options.columns;
    this.data = options.data;
  }

  createTableHeader() {
    let header = "<thead><tr>";
    for (const column of this.columns) {
      header += `<th>${column}</th>`;
    }
    header += "</tr></thead>";
    return header;
  }

  createTableBody() {
    let body = "<tbody>";
    for (const rowData of this.data) {
      body += "<tr>";
      for (const cellData of rowData) {
        body += `<td>${cellData}</td>`;
      }
      body += "</tr>";
    }
    body += "</tbody>";
    return body;
  }

  render(containerId) {
    const gridContainer = document.getElementById(containerId);
    const table = document.createElement("table");
    table.classList.add("table", "table-hover");
    table.innerHTML = this.createTableHeader() + this.createTableBody();
    gridContainer.appendChild(table);
  }
}

export default Grid;
